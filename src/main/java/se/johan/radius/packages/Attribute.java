package se.johan.radius.packages;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import se.johan.radius.meta.Type;

/*
 * Immutable class => If set methods they should return new Attribute.
 */
public final class Attribute {
	private final byte type;
	private final byte length;
	private final byte[] value;

	public Attribute(Type type, byte[] value) {
		// 2 is for type + length, each is 1 octet.
		this.length = (byte) (2 + value.length);
		this.type = type.value;
		this.value = value;
	}

	/**
	 *  Condition: length shall be correct.
	 */
	public Attribute(byte type, byte length, byte[] value) {
		this.type = type;
		this.length = length;
		this.value = value;

	}

	public int getType() {
		return type;
	}

	public byte getLength() {
		return length;
	}

	public int getIntLength() {
		return (int) length;
	}

	public byte[] getValue() {
		return value.clone();
	}

	public static int getSumLength(List<Attribute> attributes) {
		int sumLength = 0;
		for (Attribute a : attributes)
			sumLength += a.getIntLength();
		return sumLength;
	}

	public byte[] getBytes() {
		byte[] bytes = new byte[length];
		bytes[0] = type;
		bytes[1] = length;
		System.arraycopy(value, 0, bytes, 2, value.length);
		return bytes;
	}

	public static byte[] getBytes(List<Attribute> attributes) {

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		for (Attribute a : attributes)
			try {
				outputStream.write(a.getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
		return outputStream.toByteArray();
	}
}
