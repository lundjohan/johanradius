package se.johan.radius.packages;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import se.johan.radius.meta.Constants;
import se.johan.radius.util.RadiusArrays;

/* IMMUTABLE CLASS => if set method they should return new RadiusPackage.
 */
public final class RadiusPackage {
	final byte code, identifier;
	final byte[] length;
	final byte[] authenticator;

	final List<Attribute> attributes;

	public RadiusPackage(byte code, byte identifier, byte[] length,
			byte[] authenticator) {
		this(code, identifier, length, authenticator, new ArrayList<>());
	}

	public RadiusPackage(byte code, byte identifier, byte[] authenticator,
			List<Attribute> attributes) {
		this.code = code;
		this.identifier = identifier;
		this.authenticator = authenticator;
		this.attributes = attributes;
		this.length = calcLength();
	}

	/**
	 * Check should have been made that length is ok.
	 */
	public RadiusPackage(byte code, byte identifier, byte[] length,
			byte[] authenticator, List<Attribute> attributes) {
		this.code = code;
		this.identifier = identifier;
		this.length = length;
		this.authenticator = authenticator;
		this.attributes = attributes;
	}

	public RadiusPackage(RadiusPackage rp, List<Attribute> attributes) {
		this.code = rp.code;
		this.identifier = rp.identifier;
		this.length = rp.length.clone();
		this.authenticator = rp.authenticator.clone();
		this.attributes = attributes;
	}

	// l�gg denna funktion annorst�des, h�gre upp s� att du kan s�tta length
	// final!
	private byte[] calcLength() {
		int attrLength = Attribute.getSumLength(attributes);
		byte[] byteLength = RadiusArrays.shortToTwoOctets(
				(short) (Constants.PACKAGE_LENGTH_ATTR_EXCL + attrLength));
		return byteLength;
	}

	public List<Attribute> getAttributes() {
		return attributes;
	}

	// returns attribute or null if it cannot be find
	public Attribute getAttribute(int type) {
		Attribute searchAtt = null;
		for (Attribute a : attributes) {
			if (a.getType() == type) {
				searchAtt = a;
				break;
			}
		}
		return searchAtt;
	}

	public int getLengthAsInt() {
		byte[] byteArr = new byte[Integer.BYTES];
		byteArr[2] = length[0];
		byteArr[3] = length[1];
		return ByteBuffer.wrap(byteArr).getInt();

	}
	/**
	 * length must be well set since allocation of buffer depends on that. Check
	 * for this should be taken care of higher in program.
	 * 
	 * @return
	 * @throws IndexOutOfBoundsException
	 */
	public byte[] getBytes() throws IndexOutOfBoundsException {
		ByteBuffer bb = ByteBuffer.allocate(getLengthAsInt());
		bb.put(code);
		bb.put(identifier);
		bb.put(length);
		bb.put(authenticator);
		bb.put(Attribute.getBytes(attributes));
		return bb.array();

	}
	// protected abstract void createAuthenticator();

	public byte[] getAuth() {
		return authenticator;
	}

	public byte getId() {
		return identifier;
	}

	public byte getCode() {
		return code;
	}
	public byte[] getLength() {
		return length.clone();
	}

	public RadiusPackage setAttributes(List<Attribute> attributes) {
		List<Attribute> allAttributes = new ArrayList<>();
		allAttributes.addAll(this.attributes); // OK => Attribute is immutable
		allAttributes.addAll(attributes);
		return new RadiusPackage(code, identifier, length, authenticator,
				allAttributes);
	}

	/*
	 * performs shallow copy of attributes => but ok => Attribute is immutable
	 */
	public RadiusPackage setCode(byte code) {
		RadiusPackage rp = new RadiusPackage(code, this.identifier,
				this.length.clone(), this.authenticator.clone(),
				(List<Attribute>) new ArrayList<Attribute>(this.attributes));
		return rp;
	}

	public RadiusPackage copyRadiusPackage() {
		byte[] copyLength = length.clone();
		byte[] copyAuth = authenticator.clone();
		return new RadiusPackage(code, identifier, copyLength, copyAuth,
				attributes);
	}
}
