package se.johan.radius.exceptions;
/**
 * Exception thrown when something went wrong in parsing from bytes to
 * RadiusPackages. On server this generally indicates request should be silently discarded.
 * 
 * @author Johan Lund
 *
 */
public class BytesToRadiusPackageException extends Exception {

	private static final long serialVersionUID = -4764856309329942219L;

	public BytesToRadiusPackageException() {

	}

	public BytesToRadiusPackageException(String message) {
		super(message);
	}
	public BytesToRadiusPackageException(Throwable cause) {
		super(cause);
	}

}
