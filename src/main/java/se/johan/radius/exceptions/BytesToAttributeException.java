package se.johan.radius.exceptions;

/**
 * Exception thrown when something went wrong in parsing from bytes to
 * attributes. On server this generally indicates ACCESS_REJECT should be
 * thrown.
 * 
 * @author Johan Lund
 *
 */
public class BytesToAttributeException extends Exception {
	private static final long serialVersionUID = 7014125332632998465L;
	public BytesToAttributeException() {

	}
	public BytesToAttributeException(String message) {
		super(message);
	}

}
