package se.johan.radius.server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import se.johan.radius.exceptions.BytesToAttributeException;
import se.johan.radius.exceptions.BytesToRadiusPackageException;
import se.johan.radius.makersAndValidators.PackageMaker;
import se.johan.radius.makersAndValidators.PackageValidator;
import se.johan.radius.meta.Constants;
import se.johan.radius.packages.RadiusPackage;
import se.johan.radius.util.RadiusArrays;
/**
 * Sends back ACCESS-ACCEPT, ACCESS-REJECT or silently discards. 
 * @author Johan Lund
 * @since 2016-10-04 
 */
public class Server implements Runnable {
	private final PackageMaker pm;
	private final PackageValidator pv;
	private int authPort = 1812; // default
	private DatagramSocket socket = null;
	private boolean stopRequested;

	public Server(String secret) {
		pm = new PackageMaker(secret);
		pv = new PackageValidator(secret);
	}

	public void setAuthPort(int authPort) {
		this.authPort = authPort;
	}

	public void run() {
		System.err.println("Server is starting...");
		try {
			socket = new DatagramSocket(authPort);

		} catch (SocketException e) {
			e.printStackTrace();
		}
		while (!stopRequested()) {
			byte[] inBuf = new byte[Constants.MAX_LENGTH_PACKAGE];

			RadiusPackage packageNoAttr = null;
			RadiusPackage requestPackage = null;
			RadiusPackage returnPackage = null;
			DatagramPacket packet = null;
			try {
				packet = receivePacket(inBuf);
				if (!pv.nasClientIsValid(packet.getAddress()))
					continue;

				/*
				 * Dividing of RadiusPackage in two steps facilitates
				 * handling of exceptions (which decides whether discarding request or
				 * sending access-reject).
				 */
				byte[] mainPart = RadiusArrays.copyOfRange(inBuf, 0, Constants.PACKAGE_LENGTH_ATTR_EXCL);
				byte[] attributesPart = RadiusArrays.copyOfRange(inBuf, Constants.PACKAGE_LENGTH_ATTR_EXCL,
						inBuf.length);
				packageNoAttr = PackageMaker.makePackageWithoutAttr(mainPart);
				requestPackage = PackageMaker.addAttributes(packageNoAttr, attributesPart);
				if (!pv.requestCodeOK(requestPackage)) {
					System.err.println("Code not valid. Silent Discarding Request Package.");
					continue;
				}
				if (pv.passwordOK(requestPackage)) {
					returnPackage = pm.createAccessAccept(requestPackage.getId(), requestPackage.getAuth());
					System.err.println("Password OK => ACCESS-ACCEPT.");
				} else {
					returnPackage = pm.createAccessReject(requestPackage.getId(), requestPackage.getAuth());
					System.err.println("Username or Password not ok => ACCESS-REJECT.");
				}
			} catch (BytesToRadiusPackageException e) {
				System.err.println("Silent Discarding Request Package.");
				continue;
			} catch (IOException e){
				//if socket closed from stopServer, then server is quitting
				continue;
			}
			
			catch (BytesToAttributeException e) {
				returnPackage = pm.createAccessReject(packageNoAttr.getId(), packageNoAttr.getAuth());
				System.err.println("One or more Attributes broken=> ACCESS-REJECT.");
				e.printStackTrace();
			}
			// send back response
			byte[] outBuf = returnPackage.getBytes();

			InetAddress address = packet.getAddress();
			int port = packet.getPort();
			packet = new DatagramPacket(outBuf, outBuf.length, address, port);
			try {
				socket.send(packet);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.err.println("Server closed successfully");
	}

	public synchronized void stopServer() {
		stopRequested = true;
		socket.close();
	}

	private synchronized boolean stopRequested() {
		return stopRequested;
	}

	private DatagramPacket receivePacket(byte[] buf) throws IOException {
		DatagramPacket packet = new DatagramPacket(buf, buf.length);
		socket.receive(packet);
		return packet;
	}

}