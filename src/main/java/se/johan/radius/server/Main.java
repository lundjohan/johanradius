package se.johan.radius.server;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import se.johan.radius.db.DataBaseHandler;


/**
 * Runs the Server for some time. Change this time by changing parameter to
 * sleep.
 *
 */
public class Main {
	// arguments: <secret> <authport> <acctport>
	public static void main(String[] args) throws IOException, Exception {
		DataBaseHandler.startDB();
		Server server = null;
		if (args.length == 0) {
			System.err.println("Usage arguments: <secret> [<authorization port>");
			System.exit(0);
		}
		
		if (args.length >= 1)
			server = new Server(args[0]);
		if (args.length >= 2)
			server.setAuthPort(Integer.parseInt(args[1]));
		Thread t = new Thread(server);
		t.start();
		System.out.println("Server started.");
		// run server for 20 seconds.
		TimeUnit.SECONDS.sleep(20);
		server.stopServer();
		DataBaseHandler.closeDB();
	}
}