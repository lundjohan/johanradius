package se.johan.radius.makersAndValidators;

import java.net.InetAddress;
import java.util.List;

import se.johan.radius.db.DataBaseHandler;
import se.johan.radius.meta.Code;
import se.johan.radius.meta.Type;
import se.johan.radius.packages.Attribute;
import se.johan.radius.packages.RadiusPackage;
import se.johan.radius.util.RadiusArrays;
/**
 * This class is entirely used for validating various things. For example that
 * password is correct. It has contact with database.
 * 
 * @author Johan Lund
 *
 */
public class PackageValidator {
	final private DigestMachine dm;

	public PackageValidator(String secret) {
		this.dm = new DigestMachine(secret);
	}

	public boolean passwordOK(RadiusPackage requestPackage) {
		Attribute usernameAttr = requestPackage
				.getAttribute(Type.USER_NAME.value);
		if (usernameAttr == null)
			return false;

		Attribute passwordAttr = requestPackage
				.getAttribute(Type.USER_PASSWORD.value);

		if (passwordAttr == null)
			return false;

		// String password = UserPwdMatch.get(new
		// String(usernameAttr.getValue()));
		String password = DataBaseHandler
				.getPasswordFromDB(new String(usernameAttr.getValue()));
		if (password == null)
			return false;

		byte[] hashValue = dm.digestPassword(requestPackage.getAuth(),
				password.getBytes());

		return new String(passwordAttr.getValue())
				.equals(new String(hashValue));
	}

	public static boolean attributesOK(RadiusPackage rp) throws Exception {
		List<Attribute> attributes = rp.getAttributes();
		boolean okAttrs = true;
		for (Attribute a : attributes) {
			if (!attributeIsOk(a)) {
				okAttrs = false;
				break;
			}

		}
		return okAttrs;
	}

	private static boolean attributeIsOk(Attribute a) {
		// check that length property is ok
		int saidLength = (int) a.getLength();
		int realLength = 2 + a.getValue().length;
		System.err.println("Attrbute value is: " + new String(a.getValue())
				+ "saidLength: " + saidLength + "realLength " + realLength);
		return saidLength == realLength;
	}

	/**
	 * Returns true if attributes lengths are ok => accumulated length is same
	 * Parameter bytes starts with attributes and have no trailing zeros.
	 * 
	 * @param bytes
	 * @return
	 */
	public static boolean attributesLengthsOK(byte[] bytes) {
		boolean lengthOk = true;
		int i = 0;
		while (i < bytes.length) {
			int lengthAttr = attributeLengthOk(
					RadiusArrays.copyOfRange(bytes, i, bytes.length));
			if (lengthAttr == -1) {
				lengthOk = false;
				break;
			}
			i += lengthAttr;
		}
		return lengthOk;
	}

	/**
	 * If first attributes length is 3 or over and that the length of byte array
	 * after this attribute has been drawn is zero, 3 or over 3.
	 * 
	 * @param bytes
	 * @return length of first attribute in byte array. If something is wrong
	 *         return -1.
	 */
	private static int attributeLengthOk(byte[] bytes) {
		int lengthOfAttribute = -1;
		if (bytes.length >= 3 && ((int) bytes[1] == bytes.length
				|| (int) bytes[1] <= bytes.length - 3))
			lengthOfAttribute = (int) bytes[1];
		return lengthOfAttribute;
	}

	public boolean requestCodeOK(RadiusPackage requestPackage) {
		return requestPackage.getCode() == Code.ACCESS_REQUEST.value;
	}
	/**
	 * Mock-up. But this method would in a serious implementation check
	 * towards a database for valid ip-addresses and silent discard unknown
	 * addresses.
	 * 
	 * @param ipAddress
	 * @return
	 */
	public boolean nasClientIsValid(InetAddress ipAddress) {
		return true;
	}
}
