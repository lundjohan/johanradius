package se.johan.radius.makersAndValidators;

import java.util.ArrayList;
import java.util.List;

import se.johan.radius.exceptions.BytesToAttributeException;
import se.johan.radius.meta.Type;
import se.johan.radius.packages.Attribute;
import se.johan.radius.util.RadiusArrays;

/**
 * Package private class.
 * 
 * Similar to Package Maker but now for simple construction of Attributes.
 * @author Johan Lund
 *
 */
final class AttributeMaker {
	private final DigestMachine dm;

	AttributeMaker(DigestMachine dm) {
		this.dm = dm;
	}

	Attribute createUserNameAttr(String username) throws Exception {
		return new Attribute(Type.USER_NAME, username.getBytes());
	}

	Attribute createPasswordAttr(byte[] authenticator, String password) throws Exception {
		byte[] hashedPwd = dm.digestPassword(authenticator, password.getBytes());
		return new Attribute(Type.USER_PASSWORD, hashedPwd);
	}

	/**
	 * Parses bytes into attributes.  original.length cannot be smaller than lengthOfAttributes. Check for this
	 * should have been made higher up in hierarchy.
	 * 
	 * @param original
	 * @param lengthOfAttributes
	 * @return
	 * @throws BytesToAttributeException,
	 *             IllegalArgumentException
	 */
	static List<Attribute> attributesFromBytes(byte[] original, int lengthOfAttributes)
			throws BytesToAttributeException {
		List<Attribute> attributes = new ArrayList<>();
		if (lengthOfAttributes == 0)
			return attributes;
		if (original.length < lengthOfAttributes)
			throw new IllegalArgumentException(
					"original.length < lengthOfAttributes: " + original.length + " < " + lengthOfAttributes);
		//shrink
		byte[] bytes = original.length > lengthOfAttributes ? RadiusArrays.copyOfRange(original, 0, lengthOfAttributes)
				: original.clone();
			
		if (RadiusArrays.endsWithZeros(bytes))
			throw new BytesToAttributeException("Attribute ends with zero.");
		if (!PackageValidator.attributesLengthsOK(bytes))
			throw new BytesToAttributeException("Something got wrong when calculating attributes lengths.");

		int i = 0;

		while (i < bytes.length) 
		{
			if (bytes[i] == 0x00)
				throw new BytesToAttributeException("Cause: attribute type can not be zero");

			byte type = bytes[i];
			++i;
			byte attrLen = bytes[i];
			++i;
			int valueLen = attrLen - 2; // 2 bytes removed because ->
										// sizeof(type+attrLen) == 2
			if (bytes[i + valueLen - 1] == 0x00) //// -1 => index starts at zero
				throw new BytesToAttributeException(
						"Cause: Attribute.value ends with null which shouldn't be possible");

			byte[] value = RadiusArrays.copyOfRange(bytes, i, i + valueLen);
			Attribute a = new Attribute(type, attrLen, value);
			attributes.add(a);
			i += valueLen;
		}
		return attributes;

	}
}
