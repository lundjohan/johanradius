package se.johan.radius.makersAndValidators;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import se.johan.radius.exceptions.BytesToAttributeException;
import se.johan.radius.exceptions.BytesToRadiusPackageException;
import se.johan.radius.meta.Code;
import se.johan.radius.meta.Constants;
import se.johan.radius.packages.Attribute;
import se.johan.radius.packages.RadiusPackage;
import se.johan.radius.util.RadiusArrays;

/**
 * Creates packages with help of DigestMachine (password) and AttributeMaker
 * (attributes).
 * 
 * @author Johan Lund
 *
 */
public final class PackageMaker {
	private final DigestMachine dm;
	private final AttributeMaker am;

	public PackageMaker(String secret) {
		this.dm = new DigestMachine(secret);
		this.am = new AttributeMaker(dm);
	}

	/**
	 * From rfc2865: Note that none of the types in RADIUS terminate with a NUL
	 * (hex 00).In particular, types "text" and "string" in RADIUS do not
	 * terminate with a NUL (hex 00).The Attribute has a length field and does
	 * not use a terminator. Text contains UTF-8 encoded 10646 [7] characters
	 * and String contains 8-bit binary data.Servers and servers and clients
	 * MUST be able to deal with embedded nulls.
	 * 
	 * @throws BytesToRadiusPackageException
	 * @throws BytesToAttributeException
	 */
	public static RadiusPackage makePackageWithoutAttr(byte[] original)
			throws BytesToRadiusPackageException {
		if (original.length < Constants.PACKAGE_LENGTH_ATTR_EXCL)
			throw new BytesToRadiusPackageException(
					"Length of bytes should be at least "
							+ Constants.PACKAGE_LENGTH_ATTR_EXCL + " long.");
		byte code = original[0];
		byte identifier = original[1];
		byte[] length = RadiusArrays.copyOfRange(original, 2, 4);
		byte[] authenticator = RadiusArrays.copyOfRange(original, 4,
				Constants.PACKAGE_LENGTH_ATTR_EXCL);
		return new RadiusPackage(code, identifier, length, authenticator);
	}
	public static RadiusPackage addAttributes(RadiusPackage mainPackage,
			byte[] attributesPart)
			throws BytesToAttributeException, BytesToRadiusPackageException {
		int lengthAttributes = mainPackage.getLengthAsInt()
				- Constants.PACKAGE_LENGTH_ATTR_EXCL;
		byte[] attrPart = RadiusArrays.removeTrailingZeros(attributesPart);
		if (attrPart.length < lengthAttributes)
			throw new BytesToRadiusPackageException(
					"Package declare its length longer than it is");
		List<Attribute> attributes = AttributeMaker
				.attributesFromBytes(attrPart, lengthAttributes);
		return mainPackage.setAttributes(attributes);
	}

	public static RadiusPackage makePackage(byte[] inBuf)
			throws BytesToRadiusPackageException, BytesToAttributeException {
		byte[] mainPart = RadiusArrays.copyOfRange(inBuf, 0,
				Constants.PACKAGE_LENGTH_ATTR_EXCL);
		byte[] attributesPart = RadiusArrays.copyOfRange(inBuf,
				Constants.PACKAGE_LENGTH_ATTR_EXCL, inBuf.length);
		RadiusPackage packageNoAttr = PackageMaker
				.makePackageWithoutAttr(mainPart);
		return PackageMaker.addAttributes(packageNoAttr, attributesPart);
	}

	public RadiusPackage createAccessAccept(byte requestId,
			byte[] requestAuth) {
		List<Attribute> attributes = new ArrayList<>();
		return createResponseHelper(Code.ACCESS_ACCEPT, requestId, requestAuth,
				attributes);
	}

	public RadiusPackage createAccessReject(byte requestId,
			byte[] requestAuth) {
		List<Attribute> attributes = new ArrayList<>();
		return createResponseHelper(Code.ACCESS_REJECT, requestId, requestAuth,
				attributes);
	}

	public RadiusPackage createAccessRequest(String username, String password)
			throws Exception {
		// generate identifier
		byte requestId = 0x01; // this have to be made much better, see spec!
		// generate requestauth => make better, see spec!
		byte[] requestAuth = new byte[16];
		Arrays.fill(requestAuth, (byte) 0x01);

		Attribute usernameAttr = am.createUserNameAttr(username);
		Attribute pwdAttr = am.createPasswordAttr(requestAuth, password);
		// It MUST contain either a NAS-IP-Address attribute or a NAS-Identifier
		// attribute (or both).
		// Attribute nasIpAdressAttr = am.createNasIpAddrAttr();

		List<Attribute> attributes = new ArrayList<>();
		attributes.add(usernameAttr);
		attributes.add(pwdAttr);
		return createRequestHelper(Code.ACCESS_REQUEST, requestId, requestAuth,
				attributes);
	}

	private RadiusPackage createRequestHelper(Code code, byte requestId,
			byte[] requestAuth, List<Attribute> attributes) {
		byte[] length = RadiusArrays
				.shortToTwoOctets((short) (Constants.PACKAGE_LENGTH_ATTR_EXCL
						+ Attribute.getSumLength(attributes)));
		return new RadiusPackage(code.value, requestId, length, requestAuth,
				attributes);
	}

	// Only valid for Response Packages!
	private RadiusPackage createResponseHelper(Code code, byte requestId,
			byte[] requestAuth, List<Attribute> attributes) {

		byte[] length = RadiusArrays
				.shortToTwoOctets((short) (Constants.PACKAGE_LENGTH_ATTR_EXCL
						+ Attribute.getSumLength(attributes)));
		byte[] responseAuth = null;
		try {
			responseAuth = dm.digestRequestAuth(code.value, requestId, length,
					requestAuth, Attribute.getBytes(attributes));
		} catch (NoSuchAlgorithmException | IOException e) {
			e.printStackTrace();
		}
		return new RadiusPackage(code.value, requestId, length, responseAuth,
				attributes);
	}
}
