package se.johan.radius.makersAndValidators;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;

import se.johan.radius.util.RadiusArrays;

/**
 * This class takes care of the program's encryption (Message Digest of password
 * with secrets and more). Wrapper class for Secret and Digest Algorithm.
 * Immutable class. Class cannot be extended.
 * Package private class and methods.
 */
final class DigestMachine {
	private final String ALGORITHM;
	private final byte[] secret;

	DigestMachine(String secret) {
		this(secret, "MD5");
	}
	DigestMachine(String secret, String alg) {
		this.secret = secret.getBytes();
		this.ALGORITHM = alg;
	}

	// arg: toBeHashed.length == 16 bytes, password.length == 16 bytes
	byte[] hashAndXor(byte[] toBeHashed, byte[] password) {
		// byte[] password = pwd.length < 16 ? Arrays.copyOf(pwd, 16) : pwd;
		// if (toBeHashed.length != 16)
		// throw new Exception("inside xoredAndHashed. toBeHashed.length!=
		// 16.");
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance(ALGORITHM);

		} catch (NoSuchAlgorithmException e) {
			System.err.println(
					"Change algorithm for MessageDigest to a valid value!");
			e.printStackTrace();
			System.exit(0);
		}

		md.update(toBeHashed);
		byte[] hashedArr = md.digest();

		// xor with password.
		byte[] xored = new byte[16];
		for (int i = 0; i < hashedArr.length; i++)
			xored[i] = (byte) (hashedArr[i] ^ password[i]);
		return xored;
	}

	/**
	 * Parameter password should be between 16 and 128 bytes/ octets inclusive.
	 * 
	 * @param authenticator
	 * @param pwd
	 * @return
	 * @throws Exception
	 */
	byte[] digestPassword(final byte[] authenticator, final byte[] password) {
		byte[] pwd = password.clone();

		// --------------------------------------------------------------------------
		// for the first 16 bytes of password, it is only the authenticator that
		// is
		// hashed with secret (secret is added later).
		// --------------------------------------------------------------------------
		byte[] hashedAndXored = authenticator;

		// --------------------------------------------------------------------------
		// Decide how many times longer we have to hash password (16 bytes is
		// hashed at one time)
		// --------------------------------------------------------------------------
		int timesToHash = (int) Math.ceil((double) pwd.length / 16.0);

		// --------------------------------------------------------------------------
		// Decide length of last partition (its up to 16 bytes but might be
		// smaller) of password.
		// If it is smaller than 16 bytes it will later be padded with zeros to
		// the right.

		byte[] finalResult = new byte[timesToHash * 16];

		// --------------------------------------------------------------------------
		// loop over the password, partitioned with 16 bytes each (except for
		// the last partition that might be smaller
		// --------------------------------------------------------------------------
		for (int i = 0; i < timesToHash; i++) {
			byte[] toBeHashed = ArrayUtils.addAll(secret, hashedAndXored);

			// --------------------------------------------------------------------------
			// rfc2865-extract: "with the last [password partition] padded at
			// the
			// end with nulls to a 16-octet boundary".
			// -> Arrays.copyOf makes padding to zero. This only happens to last
			// partition of password array.
			// --------------------------------------------------------------------------
			byte[] pwdPartition = Arrays.copyOf(pwd, 16);
			hashedAndXored = hashAndXor(toBeHashed, pwdPartition);

			for (int j = 0; j < 16; j++)
				finalResult[16 * i + j] = hashedAndXored[j];
			// --------------------------------------------------------------------------
			// remove used partition from password array.
			// in the last partition an empty byte array will remain (this
			// doesn't matter).
			// --------------------------------------------------------------------------
			if (i != timesToHash - 1)
				pwd = RadiusArrays.copyOfRange(pwd, 16, pwd.length);
		}
		return finalResult;
	}

	// ResponseAuth = MD5(Code+ID+Length+RequestAuth+Attributes+Secret)
	byte[] digestRequestAuth(byte code, byte requestId, byte[] length,
			byte[] requestAuth, byte[] attributes)
			throws NoSuchAlgorithmException, IOException {
		MessageDigest md = MessageDigest.getInstance(ALGORITHM);

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		outputStream.write(code);
		outputStream.write(requestId);
		outputStream.write(length);
		outputStream.write(requestAuth);
		outputStream.write(attributes);
		outputStream.write(secret);
		md.update(outputStream.toByteArray());
		return md.digest();
	}
}
