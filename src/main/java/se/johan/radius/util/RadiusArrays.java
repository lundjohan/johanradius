package se.johan.radius.util;

import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * Class a bit similar to java.util.Arrays package.
 * @author Johan Lund
 *
 */
public class RadiusArrays {
	public static byte[] shortToTwoOctets(short nr) {
		return ByteBuffer.allocate(Short.BYTES).putShort(nr).array();
	}

	public static int twoOctetsToInt(byte[] twoOctets) {
		if (twoOctets.length != 2)
			throw new IllegalArgumentException();
		byte[] arr = new byte[Integer.BYTES];
		arr[2] = twoOctets[0];
		arr[3] = twoOctets[1];

		return ByteBuffer.wrap(arr).getInt();
	}

	/**
	 * Rewrite of Arrays.copyOfRange. Does NOT pad with zeros if parameter <to> is outside length of original array. Instead it will throws IllegalArgumentException
	 * @param original
	 * @param from
	 * @param to
	 * @return
	 * @throws java.lang.ArrayIndexOutOfBoundsException if from < 0 or from > original.length
	 * @throws java.lang.IllegalArgumentException if from > to OR if to - from > original.length
	 * @throws java.lang.NullPointerException if original is null
	 */
	public static byte [] copyOfRange(byte[]original, int from, int to){
		int newLength = to - from;
	       if (newLength < 0)
	           throw new IllegalArgumentException(from + " > " + to);
	       //added by Johan
	       if (newLength> original.length)
	    	   throw new IllegalArgumentException(to-from + " > " +original.length +" to - from > original.length");
	    	   
	        byte[] copy = new byte[newLength];
	        System.arraycopy(original, from, copy, 0,
	                        Math.min(original.length - from, newLength));
	        return copy;
	}
	/**
	 * If no padding zeros, a copy of the original array is returned.
	 * If all elements in original array are zeros or array is empty => empty array is returned.
	 *
	 * @param original
	 * @return
	 */
	public static byte [] removeTrailingZeros(byte [] original){
		int i= original.length-1;
		while (i>0&&original[i]==0x00){
			--i;
		}
		return Arrays.copyOf(original, i+1);
	}
	/**
	 * Parameter shall not be null.
	 * Returns false for empty array.
	 * @param bytes
	 * @return
	 */
	public static boolean endsWithZeros(byte [] bytes){
		boolean trailingZero = false;
		int length = bytes.length;
		if (length == 0)
			return false;
		if (bytes[length-1]==0x00)
			trailingZero = true;
		return trailingZero;
	}
}
