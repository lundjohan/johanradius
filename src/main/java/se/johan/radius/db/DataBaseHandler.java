package se.johan.radius.db;

import java.sql.*;

/**
 * DataBaseHandler handles the SQLite database which stores table Users (columns
 * username and passwords). In a more serious Radius Server implementation,
 * database would also contain tables Clients and Dictionaries. See
 * http://www.h3c.com.hk/Technical_Support___Documents/Technical_Documents/Switches/H3C_S5500_Series_Switches/Configuration/Operation_Manual/H3C_S5500-EI_OM_2102(V1.01)/200711/210960_1285_0.htm
 * -> I. Client/server model
 * 
 * I used SQLite because it can be embedded into an executable jar file.
 * 
 * @author Johan Lund
 * 
 */
public class DataBaseHandler {
	private static Connection c = null;
	private static Statement stmt = null;

	public static void startDB() {

		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(
					"jdbc:sqlite::resource:nexus_radius.db");
			System.out.println("Opened database successfully");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void closeDB() {
		try {
			if (stmt!= null)
				stmt.close();
			if (c!=null)
			c.close();
			System.out.println("Closed database successfully");
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}
	}

	/**
	 * returns null if username is not found in DB
	 */
	public static String getPasswordFromDB(String username) {
		String password = null;
		ResultSet rs = null;
		try {
			stmt = c.createStatement();

			rs = stmt.executeQuery(
					"SELECT password FROM users WHERE username = '" + username
							+ "';");
			if (rs.next())
				password = rs.getString("password").trim();
			rs.close();

		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}
		return password;
	}
}