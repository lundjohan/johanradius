package se.johan.radius.db;

import java.util.HashMap;
import java.util.Map;

/**
 * This class is only for testing purposes.
 * DataBaseHandler will take its place.
 * @author Johan Lund
 */
public class UserPwdMatch {
	private static Map<String, String> lookup = new HashMap<String,String>();
	static {
		lookup.put("frans1", "fran123!");
		lookup.put("frans2", "fran123!");
	}
	/**
	 * Returns the password matching the user, or null if user is unknown.
	 * @return
	 */
/*	public static String get(String usr) { 
        return lookup.get(usr); 	
   }*/
}
