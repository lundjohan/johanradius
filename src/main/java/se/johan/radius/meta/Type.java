package se.johan.radius.meta;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
/**
 * In this small program these are the only attribute types that are used.
 * @author Johan Lund
 *
 */
public enum Type {
	USER_NAME(1), USER_PASSWORD(2), NAS_IP_ADDRESS(4), NAS_PORT(5);

	public final byte value;

	private Type(int nr) {
		this.value = (byte) nr;
	}

	private static final Map<Byte, Type> lookup = new HashMap<Byte, Type>();
	static {
		for (Type t : EnumSet.allOf(Type.class))
			lookup.put(t.value, t);
	}
	public static Type get(byte code) { 
        return lookup.get(code); 	//implicit conversion byte => Byte
   }
	

}
