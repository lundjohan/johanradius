package se.johan.radius.meta;

public class Constants {	
	//PACKAGE_LENGTH_ATTRIBUTES_EXCLUSIVE
	public final static short PACKAGE_LENGTH_ATTR_EXCL = 20;
	public final static int MAX_LENGTH_PACKAGE = 4096;
}
