package se.johan.radius.meta;

/**
 * In this small program these are the only codes that are used.
 * @author Johan Lund
 *
 */
public enum Code {
	ACCESS_REQUEST(1), ACCESS_ACCEPT(2), ACCESS_REJECT(3);
	
	public final byte value;
	
	private Code(int nr){
		this.value =  (byte) nr;
	}
}
