package se.johan.radius.packages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static se.johan.radius.util.RadiusArrays.shortToTwoOctets;

import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import se.johan.radius.db.DataBaseHandler;
import se.johan.radius.meta.Code;
import se.johan.radius.meta.Type;
import se.johan.radius.server.Server;
import se.johan.radius.util.RadiusArrays;

public class ReceivingAndSendingPackagesTest {
	static final String CORRECT_NAME = "frans1";
	static final String CORRECT_PWD = "fran123!";
	static Thread threadServer = null;
	static Server server = null;
	private Client client = null;

	@BeforeClass
	public static void startServer() throws SocketException {
		DataBaseHandler.startDB();
		server = new Server("testing123");
		(new Thread(server)).start();
	}

	@Before
	public void setupClient() {
		try {
			client = new Client("localhost", 1812, "testing123");
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@After
	public void close() {
		client.stop();
	}

	@AfterClass
	public static void closeServer() {
		server.stopServer(); 
		DataBaseHandler.closeDB();
	}

	@Test
	public void clientReceivesAcceptAcceptWhenSentValidAccessRequest() throws Exception {
		RadiusPackage rp = null;
		try {
			rp = sendAccessRequest(CORRECT_NAME, CORRECT_PWD);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(Code.ACCESS_ACCEPT.value, rp.getCode());
	}

	@Test
	public void clientReceivesAcceptRejectWhenSentAttributeLengthOneOctatTooHigh() {
		RadiusPackage rp = null;
		try {
			rp = sendAccessRequestWithAttributeLengthSetOneOctateTooHigh(CORRECT_NAME, CORRECT_PWD);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals(Code.ACCESS_REJECT.value, rp.getCode());
	}

	@Test
	public void clientReceivesAcceptRejectWhenSentAttributeLengthOneOctatTooLow() {
		RadiusPackage rp = null;
		try {
			rp = sendAccessRequestWithAttributeLengthSetOneOctateTooLow(CORRECT_NAME, CORRECT_PWD);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals(Code.ACCESS_REJECT.value, rp.getCode());
	}
	@Test
	public void clientReceivesAcceptRejectWhenWrongPasswordSent(){
		RadiusPackage rp = null;
		try {
			rp = sendAccessRequest(CORRECT_NAME, "tjolevippen");
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals(Code.ACCESS_REJECT.value, rp.getCode());
	}
	@Test
	public void clientReceivesAcceptRejectWhenUsernameUnknown(){
		RadiusPackage rp = null;
		try {
			rp = sendAccessRequest("nameNotInDB", "passwordDoesn'tMatter");
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals(Code.ACCESS_REJECT.value, rp.getCode());
	}
	@Test
	public void clientReceivesAcceptRejectWhenNoAttributesIncluded(){
		RadiusPackage rp = null;
		try {
			RadiusPackage toSend = createAccessRequestWithoutAttributes();
			rp = client.sendRequest(toSend);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals(Code.ACCESS_REJECT.value, rp.getCode());
	}
	

	

	@Test
	public void clientReceivesAcceptRejectWhenNoPasswordAttributeIncluded(){
		RadiusPackage rp = null;
		try {
			RadiusPackage toSend = getAccessRequestWithoutPasswordAttribute("frans2");
			rp = client.sendRequest(toSend);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals(Code.ACCESS_REJECT.value, rp.getCode());
	}

	@Test
	public void clientReceivesSilentDiscardWhenSendingInvalidCodeField() throws Exception{
		RadiusPackage basePackage = createAccessRequest(CORRECT_NAME, CORRECT_PWD);
		byte invalidCode = (byte)255; 
		RadiusPackage newToSend = basePackage.setCode(invalidCode); //this code is reserved and should not be used.
		RadiusPackage received = null;
		received = client.sendRequest(newToSend);
		assertNull(received);
		
	}
	@Test
	public void clientReceivesSilentDiscardWhenSendingPackageWithBytesLowerThanSaid() throws Exception{
		RadiusPackage bp = createAccessRequest(CORRECT_NAME, CORRECT_PWD);
		int oneMoreLength= bp.getLengthAsInt()+1;
		byte [] oneMoreLengthBytes = RadiusArrays.shortToTwoOctets((short)oneMoreLength);
		RadiusPackage toSend = new RadiusPackage(bp.getCode(), bp.getId(), oneMoreLengthBytes, bp.getAuth(), bp.getAttributes());
				//basePackage.setLength(oneMoreLengthBytes);
		RadiusPackage received = client.sendRequest(toSend);
		assertNull(received);	
	}
	
	private RadiusPackage createAccessRequestWithoutAttributes() {
		byte requestId = 0x01; 
		byte[] requestAuth = new byte[16];
		Arrays.fill(requestAuth, (byte) 0x01);
		List<Attribute> attributes = new ArrayList<>();
		return new RadiusPackage(Code.ACCESS_REQUEST.value, requestId, requestAuth, attributes);
	}

	private RadiusPackage sendAccessRequest(String username, String pwd) throws Exception {
		return client.sendRequest(createAccessRequest(username, pwd));
	}
	private RadiusPackage createAccessRequest(String username, String pwd) throws Exception{
		return client.getPM().createAccessRequest(username, pwd);
	}
	
	private RadiusPackage getAccessRequestWithoutPasswordAttribute(String string) throws Exception {
		RadiusPackage rp = client.getPM().createAccessRequest(string, "pwdDoesntMatter");
		Attribute username = rp.getAttribute(Type.USER_NAME.value);
		List<Attribute>attributes = new ArrayList<>();
		attributes.add(username);
		return new RadiusPackage(rp.getCode(), rp.getId(), rp.getAuth(), attributes);
	}

	private RadiusPackage sendAccessRequestWithAttributeLengthSetOneOctateTooHigh(String username, String pwd)
			throws Exception {
		return sendAccessRequestWithAttributeLengthSetWrong(username, pwd, +1);
	}

	private RadiusPackage sendAccessRequestWithAttributeLengthSetOneOctateTooLow(String username, String pwd)
			throws Exception {
		return sendAccessRequestWithAttributeLengthSetWrong(username, pwd, -1);
	}

	private RadiusPackage sendAccessRequestWithAttributeLengthSetWrong(String username, String pwd, int deltaWrong)
			throws Exception {
		RadiusPackage ar1 = client.getPM().createAccessRequest(username, pwd);
		byte[] badAttrValue = "bad_length".getBytes();
		int badAttrLength = 2 + badAttrValue.length;
		// we change length of attribute with +1 => make it wrong.
		
		Attribute badAttr = new Attribute(Type.NAS_PORT.value, (byte) (badAttrLength + deltaWrong), badAttrValue);
		List<Attribute> attributes = ar1.getAttributes();
		attributes.add(badAttr);

		// length of request package shall be correct
		byte[] lengthOfPackage = shortToTwoOctets((short) (badAttrLength + ar1.getLengthAsInt()));

		RadiusPackage ar2 = new RadiusPackage(ar1.getCode(), ar1.getId(), lengthOfPackage, ar1.getAuth(), attributes);
		return client.sendRequest(ar2);
	}
}