package se.johan.radius.packages;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

import se.johan.radius.meta.Constants;

public class TestMethods {
	/**
	 * This method exists because RadiusPackage.getBytes() doesn't work for
	 * constructing package with length wrongly set.
	 */
	static byte[] getBytes(RadiusPackage rp) throws IndexOutOfBoundsException {
		ByteBuffer bb = ByteBuffer.allocate(Constants.MAX_LENGTH_PACKAGE);
		bb.put(rp.getCode());
		bb.put(rp.getId());
		bb.put(rp.getLength());
		bb.put(rp.getAuth());
		bb.put(getBytes(rp.getAttributes())); // this have to change.
		return bb.array();

	}
	static byte[] getBytes(List<Attribute> attributes)
			throws IndexOutOfBoundsException {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		for (Attribute a : attributes)
			try {
				outputStream.write(getBytesTest(a));
			} catch (IOException e) {
				e.printStackTrace();
			}
		return outputStream.toByteArray();
	}
	/**
	 * In comparision to Attribute.getBytes() this methods doesn't notice/ throw
	 * RuntimeException when attribute.value is wrong
	 */
	static byte[] getBytesTest(Attribute a) {
		int sumLength = 2 + a.getValue().length;
		ByteBuffer bb = ByteBuffer.allocate(sumLength);
		bb.put((byte) a.getType());
		bb.put(a.getLength());
		bb.put(a.getValue());
		return bb.array();
	}
}
