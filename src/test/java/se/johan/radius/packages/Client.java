package se.johan.radius.packages;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;

import se.johan.radius.exceptions.BytesToAttributeException;
import se.johan.radius.exceptions.BytesToRadiusPackageException;
import se.johan.radius.makersAndValidators.PackageMaker;

/**
 * This client is suboptimal and should NOT be used in production. It is
 * suboptimal in the sense that it uses methods that is written to lay the blind
 * eye to the parameters (RadiusPackages), for example TestMethods.getBytes. It
 * is entirely written to test a Radius Server.
 * 
 * @author Johan Lund
 *
 */
public class Client {
	private final PackageMaker pm;
	private String serverHostname = null;
	private int serverPort;
	private DatagramSocket socket = null;
	InetAddress address = null;

	public Client(String hostname, int port, String secret)
			throws SocketException {
		socket = new DatagramSocket();
		this.serverHostname = hostname;
		this.serverPort = port;
		pm = new PackageMaker(secret);
	}

	public PackageMaker getPM() {
		return pm;
	}

	public RadiusPackage sendRequest(RadiusPackage rp) throws IOException,
			BytesToRadiusPackageException, BytesToAttributeException {
		// send request
		byte[] inBuf = TestMethods.getBytes(rp); // getBytes let bad RadiusPackages through
										// without throwing exceptions.
		InetAddress address = InetAddress.getByName(serverHostname);
		DatagramPacket packet = new DatagramPacket(inBuf, inBuf.length, address,
				serverPort);
		socket.send(packet);

		// get response
		packet = new DatagramPacket(inBuf, inBuf.length);
		socket.setSoTimeout(1000);

		RadiusPackage receivedPackage = null;
		try {
			socket.receive(packet);
			// display response
			byte[] outBuf = packet.getData();
			receivedPackage = PackageMaker.makePackage(outBuf);
		} catch (SocketTimeoutException e) {
			System.out.println("Server responded with silent discard");
		}
		return receivedPackage;
	}
	

	public void stop() {
		socket.close();
	}
}