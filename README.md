#Johan Lund Radius Readme#
2016-10-04
Johan Lund
##Uppgiften##
Uppgift från NeXus: Skapa en Radius server och som svarar lämpligt på från NeXus skickad Test-NASClient.
Återkoppling
Förra inlämningen använde jag biblioteket TinyRadius. Denna gången har jag implementerat allt själv.
Bifogade filer
johanlund_radius_assignment_2.zip
häri ligger:
johan_radius.jar => executable jar
johan_radius_source => eclipse-projekt med java-filerna.
johan_radius_lib => används av executable jar (och jar filerna härinne måste inkluderas på buildpath om sourcekoden ska kunna köras)
JohanRadiusUML.png => UML-diagram över de viktigaste klasserna.
denna readme.
Använda miljöer
java 1.8 sdk
JUnit 4
Eclipse Mars
Windows 10.
##Hur använda programmet##
Executable jar är nexus_radius.jar och körs så här:
java -cp <path> johan_radius.jar se.johan.radius.server.Main <SharedSecret><[default 1812]UDP auth-port>
t ex, min executable jar ligger i mappen <johanlund_radius_assignment_2> på mitt Skrivbort. Koden blir (med shared secret ’testing123 och auth-port 1812’):
java -cp C:\Users\Johan\Desktop\johanlund_radius_assignment_2\johan_radius_jar se.johan.radius.server.Main testing123 1812
Om ingen auth port anges är default  1812.
Sen ska det bara vara att köra radiusclient.jar
Servern är uppe i 20 sekunder , sen stoppar den (ändra sleep i Main för att ändra tiden). Det kan bli problem om de körs oftare än så om man kör på samma port.
##Beskrivning av lösning##
Enklast är att innan ni läser detta ta en snabb titt på UML-diagrammet i (ligger högst upp i zip-filen): ’JohanRadiusUML.png’.
Jag har git-historiken här: https://bitbucket.org/lundjohan/johanradius/overview
OBS! Jag har denna gång medvetet INTE tittat på hur TinyRadius (som jag använde mig av förra gången) var uppbyggt under skalet. Koden bör inte vara lik. 
Jag har skrivit javadoc-kommentarer för de flesta av klasserna.
Jag beskriver nu programmet med tidigare nämnt UML-diagram framför mig (vissa av klasserna jag nämner är av mindre vikt och är ej med i UML-diagrammet).
###Testnings-klasser###
Clientthread och Client är del av testmaskineriet, vilket är mera omfattande än nexus-intervju-klienten. Jag testar t ex för om attributens längder är angivna fel (ska returnera ACCESS-REJECT).
Client är som sagt en ”testklass”. Den ska kunna skicka Radius Packages som inte är fullgoda. Det blev svårt att göra det helt och hållet med de vanliga klasserna som servern använder sig av, för de klarar inte om längdparameterna är fel satta etc (kontroll av detta ska göras högre upp i programmet). Därför finns TestMethods som innehåller en del mindre restriktiva metoder.
###Servern och makerklasserna och DigestMachine.###
Både client och servern använder sig av PackageMaker som gör det lättare att skapa packages från råa bytes. PackageValidator är en klass som kontrollerar att saker ligger rätt till. T ex kollar den att lösenordet stämmer med databasen.  
PackageMaker använder sig av AttributeMaker som underlättar läsningen från bytes till Attribute.
Jag har lite medvetet använt ordet Maker istället för Factory eftersom det inte är ngt särskilt pattern som följs, de två maker-klasserna har främst en encapsulating funktion för de lägre klasserna.
PackageValidator är den enda klassen som använder sig av databasen där username och password ligger lagrade. Den har också en mock-up-metod som används i servern: ’nasClientIsValid’. Hade denna varit seriöst implementerad hade även den tittat mot databas, men denna gång för ip-adresser för godkända NAS-klienter.
###DigestMachine###
 DigestMachine är en liten men viktig klass. Den används av PackageValidator och de två makerklasserna för krypteringen. Det är bara DigestMachine som lagrar the secret. Ingen annan klass i programmet känner egentligen till krypteringsprocessen.
För säkerhets skull är DigestMachine-klassen package private. Den är immutable. Det finns heller inga gets. Vidare är den final field och private för de klasser som har den som datamedlem. Dessa klasser är i sin tur final och private field för servern. Den är alltså inte utbytbar. 
2 egna exceptions
Både PackageMaker och AttributeMaker slänger ut varsitt Exception (ByteToRadiusException och ByteToAttributeException) om ngt är galet med byte arrayen de läser in. Jag har valt att dela upp det på två Exceptions eftersom att RFC 2865-dok tydligt beskriver att om ngt går fel i inläsningen av Radius (dess längd är galen osv) så ska inget svar skickas, men om ngt fel sker i inläsningen av ett attribut så får gärna ett ACCESS-REJECT skickas. 
Det gjorde det enkelt att när servern tog emot (catch)ett ByteToRadiusException => ignorera, och vid ByteToAttributeException => returnera ACCESS-REJECT.
RadiusPackage och Attribute
RadiusPackage är själva grundpaketet allt handlar om. Den har attributes. Bägge dessa klasser är immutables. 
###Övrigt – RadiusArrays.###
Jag har också en ren hjälpklass för hantering av byte arrays som är rikligt förekommande i programmet. Jag ville undvika padding av nollor som t ex Arrays. Jag copy-pastade t ex Arrays.copyOfRange hit, men slänger ut ett RuntimeException om man vill kopiera längre än vad arrayen har elements. (Originalet hade fyllt ut med nollor vilket inte fungerade bra för min Radius-implementationen).